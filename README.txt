# colorbutton_styletoattribute
Changes CKEditor Color Button to use attributes instead of style tags for adding color to text / backgrounds. As you can experience issues with Views / other use cases when trying to use style tags as these get stripped out.
See: https://www.drupal.org/project/colorbutton/issues/2910028 - for inspiration to create this module.

